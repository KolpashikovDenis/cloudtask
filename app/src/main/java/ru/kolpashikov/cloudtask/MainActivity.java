package ru.kolpashikov.cloudtask;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import ru.kolpashikov.cloudtask.CommonClasses.OnFragmentEventListener;
import ru.kolpashikov.cloudtask.Fragments.ListTasksFragment;
import ru.kolpashikov.cloudtask.Fragments.LoginFragment;
import ru.kolpashikov.cloudtask.Fragments.RegistrationFragment;

public class MainActivity extends AppCompatActivity implements OnFragmentEventListener {

    private String LOG = "_logs";

    private LoginFragment loginFragment;
    private RegistrationFragment regFragment;
    private ListTasksFragment tasksFragment;
    FragmentTransaction fTrans;

    private FirebaseAuth m_Auth;
    private FirebaseUser m_User;
    private FirebaseAuth.AuthStateListener authListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loginFragment = new LoginFragment();
        regFragment = new RegistrationFragment();
        tasksFragment = new ListTasksFragment();

        m_Auth = FirebaseAuth.getInstance();
        authListener = new FirebaseAuth.AuthStateListener() {

            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                fTrans = getSupportFragmentManager().beginTransaction();
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
//                    fTrans.replace(R.id.main_layout, tasksFragment);
                } else {
//                    fTrans.replace(R.id.main_layout, loginFragment);
//                    Toast.makeText(getBaseContext(), "Пользователь вышел", Toast.LENGTH_SHORT).show();
                }
                fTrans.commit();
            }
        };

        m_User = FirebaseAuth.getInstance().getCurrentUser();

        fTrans = getSupportFragmentManager().beginTransaction();
        if (m_User != null) {
            fTrans.add(R.id.main_layout, tasksFragment);
        } else {
            fTrans.add(R.id.main_layout, loginFragment);
        }
        fTrans.commit();
//        fTrans = getSupportFragmentManager().beginTransaction();
//        fTrans.add(R.id.main_layout,loginFragment);
//        fTrans.commit();
    }

    @Override
    protected void onStart() {
        super.onStart();
        m_Auth.addAuthStateListener(authListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (authListener != null) {
            m_Auth.removeAuthStateListener(authListener);
        }
    }

    @Override
    public void onEventListener(int kindEvent, Object arg) {
        Bundle b = (Bundle) arg;
        String email = b.getString("email");
        String password = b.getString("password");
        Log.d(LOG, "Вызвана onEventListener");
        switch (kindEvent) {
            case R.id.btnLogin:
                Log.d(LOG, "Клик на кнопке LOGIN");
                m_Auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(MainActivity.this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d(LOG, "Попытка авторизации");
                                if (task.isSuccessful()) {
                                    Log.d(LOG, "Авторизация успешна");
                                    fTrans = getSupportFragmentManager().beginTransaction();
                                    fTrans.replace(R.id.main_layout, tasksFragment);
                                    fTrans.commit();
                                } else {
                                    Toast.makeText(MainActivity.this, "Авторизация провалена. Ой!",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                break;

            case R.id.btnRegistration:
                /*
                regFragment.setArguments((Bundle)arg);
                fTrans = getSupportFragmentManager().beginTransaction();
                fTrans.replace(R.id.main_layout, regFragment);
                fTrans.commit(); */
                Log.d(LOG, "Клик на кнопке REGISTRATIOn");
                m_Auth.createUserWithEmailAndPassword(email, password)
                        .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                Log.d(LOG, "вызов addOnCompleteListener");
                                if (task.isSuccessful()) {
                                    Toast.makeText(MainActivity.this, "Ты успешно зарегался",
                                            Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(MainActivity.this, "Что-то не так с регистрацией",
                                            Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                break;
        }
    }


}
