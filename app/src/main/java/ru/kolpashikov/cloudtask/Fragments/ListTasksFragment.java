package ru.kolpashikov.cloudtask.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import ru.kolpashikov.cloudtask.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class ListTasksFragment extends Fragment  {
    private String LOG = "_logs";

    private FirebaseAuth m_Auth;
    private DatabaseReference dbRef;
//    private FirebaseListAdapter m_Adapter;
    private FirebaseUser user;
    private List<String> listTasks;

//    private ListView mainListView;
    private Button btnSend;
    private EditText etTask;
    FirebaseRecyclerAdapter<String, TaskViewHolder> m_Adapter;

    private static class TaskViewHolder extends RecyclerView.ViewHolder{

        TextView m_tvTitle;
        Button m_btnDel;

        public TaskViewHolder(View itemView) {
            super(itemView);
            m_tvTitle = (TextView)itemView.findViewById(R.id.tv_title_task);
            m_btnDel = (Button)itemView.findViewById(R.id.btn_del);
        }
    }

    public ListTasksFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);

        dbRef = FirebaseDatabase.getInstance().getReference();
        user = m_Auth.getInstance().getCurrentUser();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_list_tasks, container, false);
        //mainListView = (ListView)view.findViewById(R.id.listTasks);
        etTask = (EditText)view.findViewById(R.id.etTask);
        btnSend = (Button)view.findViewById(R.id.btnSend);
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dbRef.child(user.getUid()).child("Tasks").push().setValue(etTask.getText().toString());
                etTask.setText(null);
            }
        });

        RecyclerView rvListTasks = (RecyclerView)view.findViewById(R.id.rv_list_tasks);
        rvListTasks.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvListTasks.setHasFixedSize(true);

        m_Adapter = new FirebaseRecyclerAdapter<String, TaskViewHolder>(
                String.class, R.layout.task_layout, TaskViewHolder.class,
                dbRef.child(user.getUid()).child("Tasks")
        ) {
            @Override
            protected void populateViewHolder(TaskViewHolder viewHolder, String model, final int position) {
                viewHolder.m_tvTitle.setText(model);
                viewHolder.m_btnDel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        DatabaseReference itemRef = getRef(position);
                        itemRef.removeValue();
                    }
                });
            }
        };

        rvListTasks.setAdapter(m_Adapter);

        return view;
    }

    @Override
    public void onStart() {
        super.onStart();

//        m_Adapter = new FirebaseListAdapter<String>(getActivity(), String.class, android.R.layout.simple_list_item_1,
//                dbRef.child(user.getUid()).child("Tasks")) {
//            @Override
//            protected void populateView(View v, String s, int position) {
//                TextView text = (TextView)v.findViewById(android.R.id.text1);
//                text.setText(s);
//            }
//        };
//
//        mainListView.setAdapter(m_Adapter);
        /*dbRef.child(user.getUid()).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                GenericTypeIndicator<List<String>> gtIndicator = new GenericTypeIndicator<List<String>>() {};
                Log.d(LOG, "Created generic");
                listTasks = dataSnapshot.child("Tasks").getValue(gtIndicator);
                Log.d(LOG, "getted \"listTasks\"");
                if(listTasks != null) {
                    Log.d(LOG, "en");
                    updateUI();
                } else {
                    Toast.makeText(getContext(), "Tasks is empty", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });*/

    }

    private void updateUI() {
//        ArrayAdapter<String> adapter =
//                new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, listTasks);
//        mainListView.setAdapter(adapter);
    }

}
