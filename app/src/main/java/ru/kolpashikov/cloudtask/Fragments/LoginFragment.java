package ru.kolpashikov.cloudtask.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import ru.kolpashikov.cloudtask.CommonClasses.OnFragmentEventListener;
import ru.kolpashikov.cloudtask.R;

public class LoginFragment extends Fragment implements View.OnClickListener {

    private OnFragmentEventListener mListener;

    Button btnLogin;
    Button btnRegistration;
    EditText etEmail;
    EditText etPassword;

    public LoginFragment() {
        // Required empty public constructor
    }
//
//    public static LoginFragment newInstance(String param1, String param2) {
//        LoginFragment fragment = new LoginFragment();
//        Bundle args = new Bundle();
//        args.putString(ARG_PARAM1, param1);
//        args.putString(ARG_PARAM2, param2);
//        fragment.setArguments(args);
//        return fragment;
//    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        btnLogin = (Button)view.findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
        btnRegistration = (Button)view.findViewById(R.id.btnRegistration);
        btnRegistration.setOnClickListener(this);

        etEmail = (EditText)view.findViewById(R.id.etEmail);
        etPassword = (EditText)view.findViewById(R.id.etPassword);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentEventListener) {
            mListener = (OnFragmentEventListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentEventListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        int item = view.getId();
        Bundle b = new Bundle();
        b.putString("email",etEmail.getText().toString());
        b.putString("password", etPassword.getText().toString());
        mListener.onEventListener(item, b);

        /*switch(item){
            case R.id.btnLogin:
                mListener.onEventListener(item, null);
                break;
            case R.id.btnRegistration:

                Доделаем потом, сначала нужно сделать по уроку

                mListener.onEventListener(R.id.btnRegistration, b);
                mListener.onEventListener(item, null);
                break;
        }*/
    }
}
