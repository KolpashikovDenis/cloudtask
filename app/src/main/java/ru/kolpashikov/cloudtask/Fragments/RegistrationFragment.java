package ru.kolpashikov.cloudtask.Fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import ru.kolpashikov.cloudtask.CommonClasses.OnFragmentEventListener;
import ru.kolpashikov.cloudtask.R;


public class RegistrationFragment extends Fragment {

    private OnFragmentEventListener mListener;
    EditText etRegEmail;
    EditText etRegPassword;
    EditText etRegRepassword;

    public RegistrationFragment() {
        // Must be empty
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_registration, container, false);
        etRegEmail = (EditText)view.findViewById(R.id.etRegEmail);
        etRegPassword = (EditText)view.findViewById(R.id.etRegPassword);
        etRegRepassword = (EditText)view.findViewById(R.id.etRegRePassword);
        Bundle b = this.getArguments();
        if( b != null) {
            etRegEmail.setText(b.getString("email"));
            etRegPassword.setText(b.getString("password"));
            etRegRepassword.setText(b.getString("password"));
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentEventListener) {
            mListener = (OnFragmentEventListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentEventListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

}
