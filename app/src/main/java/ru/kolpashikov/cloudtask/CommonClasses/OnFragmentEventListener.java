package ru.kolpashikov.cloudtask.CommonClasses;

/**
 * Created by Denis on 22.07.2017.
 */

public interface OnFragmentEventListener {
    public void onEventListener(int kindEvent, Object arg);
}
